package Server;

import javax.swing.JFrame;

public class ServerTest {

    public static void main(String[] args) {
        Server chattServer = new Server();
        chattServer.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        chattServer.startRunning();
    }
}
