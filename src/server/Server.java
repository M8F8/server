package Server;

import java.io.*;
import java.net.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Server extends JFrame {

    private JTextField userText;
    private JTextArea chatWindow;
    private ObjectOutputStream output;
    private ObjectInputStream input;
    private ServerSocket server;
    private Socket connection;

    //konstruktor
    public Server() {
        super("Server");
        userText = new JTextField();
        userText.setEditable(false);
        userText.addActionListener(
                new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                sendMessage(event.getActionCommand());
                userText.setText("");
            }
        }
        );
        add(userText, BorderLayout.NORTH);
        chatWindow = new JTextArea();
        add(new JScrollPane(chatWindow));
        setSize(300, 150); //Anger storlek på rutan
        setVisible(true);
    }

    //Köra servern
    public void startRunning() {
        try {
            server = new ServerSocket(6789, 100);
            while (true) {
                try {
                    //Försöker ansluta och starta en konversation
                    waitForConnection();
                    setupStreams();
                    whileChatting();
                } catch (EOFException eofException) {
                    showMessage("\n Servern avslutade anslutningen. ");
                } finally {
                    closeFunctions();
                }
            }
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }
// vänta på anslutning, visa sedan anslutningsinformation

    private void waitForConnection() throws IOException {
        showMessage("Väntar på att någon skall ansluta...\n");
        connection = server.accept();
        showMessage("Ansluten till " + connection.getInetAddress().getHostName());

    }

    //sänder och tar emot data via stream
    private void setupStreams() throws IOException {
        output = new ObjectOutputStream(connection.getOutputStream());
        output.flush();
        input = new ObjectInputStream(connection.getInputStream());
        showMessage("\n Streams är nu klara! \n");
    }

    //under konversationen
    private void whileChatting() throws IOException {
        String message = "Du är nu ansluten. ";
        sendMessage(message);
        ableToType(true);
        do {
            try {
                message = (String) input.readObject();
                showMessage("\n" + message);
            } catch (ClassNotFoundException classNotFoundException) {
                showMessage("\n Okänt objekt skickat. ");
            }
        } while (!message.equals("CLIENT - END"));
    }

    //Stäng streams och socket när konversationen är klar
    private void closeFunctions() {
        showMessage("\n Avslutar anslutningar... \n");
        ableToType(false);
        try {
            output.close();
            input.close();
            connection.close();
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }

    //Skicka meddelande till klient
    private void sendMessage(String message) {
        try {
            output.writeObject("SERVER - " + message);
            output.flush();
            showMessage("\n SERVER - " + message);
        } catch (IOException ioException) {
            chatWindow.append("\n ERROR: KAN INTE SKICKA MEDDELANDE.");
        }
    }

    //Uppdaterar chatrutan
    private void showMessage(final String text) {
        SwingUtilities.invokeLater(
                new Runnable() {
            public void run() {
                chatWindow.append(text);
            }
        }
        );
    }

    //Tillåter att användaren får skriva saker i chattrutan
    private void ableToType(final boolean tof) {
        SwingUtilities.invokeLater(
                new Runnable() {
            public void run() {
                userText.setEditable(tof);
            }
        }
        );
    }
}
